const MLS_PER_MIN = 60 * 1000;
const MLS_PER_HOUR = 60 * 60 * 1000;
const MLS_PER_DAY = 24 * 60 * 60 * 1000;
const DEFAULT_COUNTDOWN_RANGE = 1000;

function getTimeElements(basedate, countdown){
  if (basedate && countdown){
    let diff = countdown - basedate;

    var days = Math.floor(diff / MLS_PER_DAY);
    var hours = Math.floor((diff % MLS_PER_DAY) / MLS_PER_HOUR);
    var minutes = Math.floor((diff % MLS_PER_HOUR) / MLS_PER_MIN);
    var secords = Math.floor((diff % MLS_PER_MIN) / 1000);

    return {
      "days": formatText(days),
      "hours": formatText(hours),
      "minutes": formatText(minutes),
      "seconds": formatText(secords)
    }
  }
}

function showTime(o){
  $("#days").text(o.days);
  $("#hours").text(o.hours);
  $("#mins").text(o.minutes);
  $("#secs").text(o.seconds);
}

function getCountDown(milliseconds){
  let seed = Math.random() * 2;
  return milliseconds + (seed * MLS_PER_DAY);
}

function updateCowndown(basedate, countdown, updateRange){
  let temp = countdown - updateRange;
  let time = getTimeElements(basedate, countdown);

  showTime(time);
  window.countdown = temp;
}

function formatText(number){
  return (number < 10)? "0" + number: number;
}

function showDates(basedate, countdown){
  $("#currDate").text(new Date(basedate).format("mm/dd/yyyy hh:MM:ss"));
  $("#cdDate").text(new Date(countdown).format("mm/dd/yyyy hh:MM:ss"));
}

(function(){
    $("document").ready(function(){
      //Get current time in milliseconds
      window.basedate = Date.now();

      //Calculate random cowndown date
      window.countdown = getCountDown(window.basedate);

      //Parse time elements to be displayed base on both dates
      let time = getTimeElements(window.basedate, window.countdown);

      //Show both above dates
      showDates(window.basedate, window.countdown)

      //Show calculated countdown remaining time
      showTime(time);

      //Set interval every second to countdown to target date.
      setInterval("updateCowndown(window.basedate, window.countdown, DEFAULT_COUNTDOWN_RANGE)", 1000);
    });
})();
