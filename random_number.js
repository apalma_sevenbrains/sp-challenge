const MIN_RANGE = 0;
const MAX_RANGE = 1000000;

function flip() {
  return Math.random() >= 0.5;
}

/** Generate decimal number parsing the input into binary representation **/
function generateRandom(n){
  let bin = (n >>> 0).toString(2);
  let gen = new String();

  for (var i = 0; i < bin.length; i++){
    gen = gen + (flip()? "1": "0");
  }

  return parseInt(gen, 2).toString(10);;
};

/** Calls generateRandom till get a random number less o equals to n **/
function randomNumber(n){
  if (n <= MIN_RANGE){
    throw Error("n must be greater than 0");
  }

  if (n >= MAX_RANGE){
    throw Error("n must be less than 1,000,000");
  }

  do {
    var generated = generateRandom(n);
  } while (generated > (n - 1));

  return generated;
}
